# 2022_05_ESTRO



## Information

Here you can find the presentation from the group at the ESTRO 2022 conference in Copenhagen, Denmark. 
You can find all the individual presentations as pdf files.



## Presentations
_'Auto-segmentation of low contrast organs at risk improves with minimal prior delineation
input'_ - Mathis Rasmussen, proffered paper

_'Uncertainty map for error prediction in deep learning-based head and neck tumor autosegmentation'_ - Jintao Ren, Proffered paper

_'Impact of guidelines on nationwide breast cancer treatment planning practices (DBCG
RT Nation study)'_ - Lasse Refsgaard, Proffered paper

_'Parametrization of artery delineation and nationwide implementation in the DBCG RT Nation cohort'_ - Emma Skarsø Buhl, proffered paper